FROM ubuntu AS builder

RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y nginx
RUN apt-get install -y python3 python3-pip
RUN apt-get install -y libmysqlclient-dev

COPY requirements.txt requirements.txt
RUN pip3 install --user -r requirements.txt

FROM builder
COPY --from=builder /root/.local /root/.local
ENV PATH=/root/.local/bin:$PATH

WORKDIR code
COPY . .
COPY nginx.conf /etc/nginx/sites-enabled/

ENV GOOGLE_APPLICATION_CREDENTIALS "firebase_accountkey.json"

EXPOSE 80

ENTRYPOINT ["./startscript.sh"]