from django.test import TestCase, Client


# Create your tests here.
from rest_framework.test import APIClient

from date.models import Person,Like
from datetime import datetime
import json


class PersonTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.person1 = Person(first_name = "Bas", middle_name = "van", last_name = "IJzendoorn", age = 15)
        self.person1.save()
        self.person1Json = {
            "id": self.person1.id,
            "first_name": self.person1.first_name,
            "middle_name": self.person1.middle_name,
            "last_name": self.person1.last_name,
            "age": self.person1.age
        }
        self.person2 = Person(first_name = "Edward", last_name = "IJzendoorn", age = 38)
        self.person2.save()
        self.person2Json = {
            "id": self.person2.id,
            "first_name": self.person2.first_name,
            "middle_name": "",
            "last_name": self.person2.last_name,
            "age": self.person2.age
        }

    def test_getFirstPerson(self):
        response = self.client.get('/persons/1/')
        assert response.status_code == 200
        self.assertEqual(response.data, self.person1Json)

    def test_getSecondPerson(self):
        response = self.client.get('/persons/2/')
        assert response.status_code == 200
        self.assertEqual(response.data, self.person2Json)

    def test_getAllPersons(self):
        response = self.client.get('/persons/')
        assert response.status_code == 200
        self.assertEqual(response.data, [self.person1Json,self.person2Json])

    def test_postPerson(self):
        self.person3Json = {
            "first_name": "Voornaam",
            "middle_name": "MiddleName",
            "last_name": "Achternaam",
            "age": 15,
        }
        response = self.client.post('/persons/', self.person3Json, format='json')
        self.person3Json.update({"id": 3})
        assert response.status_code == 201
        self.assertEqual(response.data, self.person3Json)

    def test_postWrongPerson(self):
        self.person3Json = {
            "had": "ga",
        }
        response = self.client.post('/persons/', self.person3Json)
        assert response.status_code == 400

    def test_putPerson(self):
        person2JsonPut = {
            "first_name": "putBas",
            "last_name": "putLastName",
            "age": 14,
        }
        response = self.client.put('/persons/2/', person2JsonPut, format='json')
        assert response.status_code == 200
        person2JsonPut.update({"id": 2, "middle_name": ""})
        self.assertEqual(response.data, person2JsonPut)

    def test_patchPerson(self):
        person2JsonPatch = {
            "first_name": "patchBas",
        }
        response = self.client.patch('/persons/2/', person2JsonPatch)
        self.person2Json.update(person2JsonPatch)
        assert response.status_code == 200
        self.assertEqual(response.data, self.person2Json)

    def test_postAndGetPerson(self):
        self.person3Json = {
            "first_name": "Voornaam",
            "middle_name": "MiddleName",
            "last_name": "Achternaam",
            "age": 15,
        }
        response = self.client.post('/persons/', self.person3Json, format='json')
        self.person3Json.update({"id": 3})
        assert response.status_code == 201
        self.assertEqual(response.data, self.person3Json)
        response = self.client.get('/persons/3/')
        assert response.status_code == 200
        self.assertEqual(response.data, self.person3Json)

    def test_getUnkownPerson(self):
        response = self.client.get('/persons/30/')
        assert response.status_code == 404

    def test_deletePerson(self):
        response = self.client.delete('/persons/1/')
        assert response.status_code == 204
        getResponse = self.client.get('/persons/')
        assert getResponse.status_code == 200
        self.assertEqual(len(getResponse.data), 1)

class MatchTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.person1 = Person(first_name = "Bas", middle_name = "van", last_name = "IJzendoorn", age = 15)
        self.person1.save()
        self.person2 = Person(first_name = "Edward", last_name = "IJzendoorn", age = 38)
        self.person2.save()
        self.person3 = Person(first_name= "Frans", age= 17)
        self.person3.save()
        self.datetime = datetime.now()
        Like(liked_by=self.person1, liked_person=self.person2, datetime = self.datetime).save()
        Like(liked_by=self.person1, liked_person=self.person3, datetime = self.datetime).save()
        Like(liked_by=self.person2, liked_person=self.person1, datetime = self.datetime).save()

    def idsInLikeList(self, ids, likeList):
        likeList_ids = [like["person_id"] for like in likeList]
        for id in ids: assert id in likeList_ids

    def test_getLikes(self):
        response = self.client.get('/persons/1/likes/')
        assert response.status_code == 200
        assert "likes" in response.data
        assert "liked_by" in response.data
        assert "matches" in response.data
        self.idsInLikeList([2,3], response.data["likes"])
        self.idsInLikeList([2], response.data["liked_by"])
        self.idsInLikeList([2], response.data["matches"])
        assert self.time.strftime("%m/%d/%Y, %H:%M:%S") == response.data["likes"][0]["time"]

    def test_postLike(self):
        response = self.client.post('/persons/3/like', {'id': 1}, format='json')
        assert response.status_code == 201
        self.assertEqual(len(Like.objects.all()), 4)
        resultJson = json.loads(response.data)
        assert "likes" in resultJson
        assert "liked_by" in resultJson
        assert "matches" in resultJson
        self.idsInLikeList([1], resultJson["likes"])
        self.idsInLikeList([1], resultJson["liked_by"])
        self.idsInLikeList([1], resultJson["matches"])


    def test_postUnlike(self):
        response = self.client.delete('/persons/1/matches', {'id': 2}, format='json')
        assert response.status_code == 204
        resultJson = json.loads(response.data)
        assert "likes" in resultJson
        assert "liked_by" in resultJson
        assert "matches" in resultJson
        self.idsInLikeList([3], resultJson["likes"])
        assert 0 == len(resultJson["liked_by"])
        assert 0 == len(resultJson["matches"])

    def test_getInvalidPerson(self):
        response = self.client.get('/persons/25/matches')
        assert response.status_code == 404

    def test_postInvalidPerson(self):
        response = self.client.post('/persons/25/matches')
        assert response.status_code == 404

    def test_deleteInvalidPerson(self):
        response = self.client.delete('/persons/25/matches')
        assert response.status_code == 404

    # def test_getLikedBy(self):
    #     expectedResponse = {
    #         "likes": [],
    #         "liked_by": [1],
    #         "matches": [],
    #     }
    #     response = self.client.get('/persons/2/matches')
    #     assert response.status_code == 200
    #     self.assertEqual(response.data, expectedResponse)

    # def test_matches(self):
    #     self.person2.likes = [2]
    #     self.person2.save()
    #     expectedResponse = {
    #         "likes": [2],
    #         "liked_by": [2],
    #         "matches": [2],
    #     }
    #     response = self.client.get('/persons/1/matches')
    #     assert response.status_code == 200
    #     self.assertEqual(response.data, expectedResponse)
