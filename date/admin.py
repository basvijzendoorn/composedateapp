from django.contrib import admin
from .models import Person, Like, Message

admin.site.register(Person)
admin.site.register(Like)
admin.site.register(Message)
# Register your models here.
