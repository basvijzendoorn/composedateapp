from rest_framework import serializers

from date.models import Person, Like, Message

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['id', 'from_person', 'to_person', 'date', 'message']

class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ['id', 'show', 'first_name', 'gender', 'date_of_birth', 'occupation', 'profile', 'image_1', 'image_2', 'image_3', 'image_4', 'image_5', 'image_6']

class MatchesLikeSerializer(serializers.Serializer):
    like_id = serializers.IntegerField()
    person_id = serializers.IntegerField()
    datetime = serializers.DateTimeField(format = "%Y-%m-%d %H:%M:%S")

class LikeSerializer(serializers.HyperlinkedModelSerializer):
    datetime = serializers.DateTimeField(format = "%Y-%m-%d %H:%M:%S")
    class Meta:
        model = Like
        fields = ['id','liked_person','liked_by','datetime']

class MatchesSerializer(serializers.Serializer):
    likes = MatchesLikeSerializer(many=True)
    liked_by = MatchesLikeSerializer(many=True)
    matches = MatchesLikeSerializer(many=True)