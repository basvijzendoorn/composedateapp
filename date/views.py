from django.core.files.temp import NamedTemporaryFile
from django.db.models import F
from django.http import Http404
from rest_framework.response import Response
from datetime import datetime, date

# Create your views here.
from rest_framework import viewsets
from rest_framework.decorators import action

from date.models import Person, Like, Message
from date.serializers import PersonSerializer, MessageSerializer
from date.serializers import MatchesSerializer
from date.serializers import LikeSerializer

from PIL import Image
from django.core.files.base import ContentFile
import requests
import io

from firebase_admin import messaging


class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def perform_create(self, serializer):
        message: Message = serializer.save(date=datetime.now())

        fcm_message = messaging.Message(
            notification=messaging.Notification(message.from_person.first_name, message.message),
            token=message.to_person.fcm_token,
            data={
                "click_action": "FLUTTER_NOTIFICATION_CLICK",
                "id": str(message.pk),
                "from_person": str(message.from_person.pk),
                "to_person": str(message.to_person.pk),
                "date": str(message.date) + "Z",
                "message": str(message.message)
            },
        )
        try:
            messaging.send(fcm_message)
        except:
            pass
        return Response(message)

    @action(methods=['post'], detail=False)
    def chat(self, request):
        try:
            person_1 = Person.objects.get(user=request.user)
        except:
            raise PermissionError()
        person_2 = request.POST['other_person']
        messages = (Message.objects.filter(from_person=person_1, to_person=person_2) | Message.objects.filter(
            from_person=person_2, to_person=person_1)).order_by('-date')
        messagesSerializer = MessageSerializer(messages, many=True)
        return Response(messagesSerializer.data)


class LikeViewSet(viewsets.ModelViewSet):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer


class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_class = PersonSerializer

    def perform_update(self, serializer):
        person: Person = serializer.save()
        if not person.show and bool(person.image_1) and bool(person.date_of_birth) and bool(person.first_name) and bool(
                person.gender) and bool(person.occupation):
            serializer.save(show=True)
        return Response(serializer.data)

    @action(methods=['patch'], detail=True)
    def imageURL(self, request, pk=None):
        try:
            # person = Person.objects.get(user=request.user)
            person = Person.objects.get(pk = pk)
        except:
            return Response(status=401)

        for index in range(1,6):
            try:
                url = request.data['image_' + str(index)]
                downloaded_image = requests.get(url)
                image = Image.open(io.BytesIO(downloaded_image.content))
                output_image_stream = io.BytesIO()
                image.save(output_image_stream, format="JPEG")
                output_image_file = ContentFile(output_image_stream.getvalue(), person.first_name + '_image_' + str(index) + ".JPEG")
                setattr(person, 'image_' + str(index), output_image_file)
            except: pass
        person.save()

    @action(methods=['post'], detail=False)
    def login(self, request):
        fcm_token = request.POST['fcm_token']
        try:
            person = Person.objects.get(user=request.user)
            person.fcm_token = fcm_token
            person.save()
        except:
            person = Person(
                user=request.user,
                fcm_token=fcm_token,
                date_of_birth=date.today(),
                show=False
            )
            person.save()
        serializer = PersonSerializer(person)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def logout(self, request):
        try:
            person = Person.objects.get(user=request.user)
            person.fcm_token = ""
            person.save()
            serializer = PersonSerializer(person)
            return Response(serializer.data)
        except:
            return Http404

    @action(methods=['get'], detail=True)
    def likes(self, request, pk=None):
        try:
            person = Person.objects.get(pk=pk)
        except:
            return Http404
        likes = person.likes.annotate(person_id=F("liked_person"), like_id=F("id")).values("like_id", "person_id",
                                                                                           "datetime")
        liked_by = person.liked_by.annotate(person_id=F("liked_by"), like_id=F("id")).values("like_id", "person_id",
                                                                                             "datetime")
        mtches = likes.filter(person_id__in=liked_by.values_list("person_id"))
        serializer = MatchesSerializer({
            "likes": likes,
            "liked_by": liked_by,
            "matches": mtches,
        })
        return Response(serializer.data)
