from django.db import models
from django.conf import settings

# Create your models here.
from django.contrib.auth.models import User

class Person(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    fcm_token = models.CharField(max_length = 163, default="")
    date_of_birth = models.DateField(default=0, null=True)
    show = models.BooleanField(default=False)
    first_name = models.CharField(max_length = 100, default="", blank=True)
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female')
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, default="F", blank=True)
    occupation = models.CharField(max_length = 100, default="", blank=True)
    profile = models.TextField(default="", blank=True)
    image_1 = models.ImageField(blank=True)
    image_2 = models.ImageField(blank=True)
    image_3 = models.ImageField(blank=True)
    image_4 = models.ImageField(blank=True)
    image_5 = models.ImageField(blank=True)
    image_6 = models.ImageField(blank=True)

    def __str__(self): return str(self.user)
    def __repr__(self): return self.__str__()

class Like(models.Model):
    liked_person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='liked_by')
    liked_by = models.ForeignKey(Person, on_delete=models.CASCADE, related_name='likes')
    datetime = models.DateTimeField()
    def __str__(self):
        return "Person " + str(self.liked_by.pk) + " likes " + str(self.liked_person.pk)

class Message(models.Model):
    from_person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name="messages_send")
    to_person = models.ForeignKey(Person, on_delete=models.CASCADE, related_name="messages_received")
    message = models.CharField(max_length=600)
    date = models.DateTimeField()
    # received_date = models.DateTimeField(blank=True)
    # to_received = models.BooleanField(blank=True, default=False)

    def __str__(self): return str(self.from_person) + " to " + str(self.to_person)
    def __repr__(self): return self.__str__()